# SQL语句的执行计划分析与优化指导实验

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验环境

- 数据库：Oracle 12c
- 数据库名称：pdborcl
- 用户名：sys、hr

## 实验内容

1. 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
2. 设计自己的查询语句，并作相应的分析，查询语句不能太简单。
3. 执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。
4. 将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验步骤

### 1. 授权

在开始实验之前，需要先授权hr用户，以便查看执行计划。

```sql
-- 授权
sqlplus sys/123@localhost/pdborcl as sysdba

-- 运行plustrce.sql脚本
@$ORACLE_HOME/sqlplus/admin/plustrce.sql

-- 创建角色plustrace
create role plustrace;

-- 授权
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;

-- 授权
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr;
```

### 2. 查询分析

#### 查询1

查询两个部门('IT'和'Sales')的部门总人数和平均工资。

```sql
SELECT d.department_name, count(e.job_id) as "部门总人数", avg(e.salary) as "平均工资"
FROM hr.departments d, hr.employees e
WHERE d.department_id = e.department_id
AND d.department_name IN ('IT', 'Sales')
GROUP BY d.department_name;
```

执行结果：

```
DEPARTMENT_NAME     部门总人数       平均工资
-------------------- ---------- ----------
IT                            5       5760
Sales                        34 8955.88235
```

执行计划：

```
Plan hash value: 4104853442

--------------------------------------------------------------------------------------
| Id  | Operation                    | Name         | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT             |              |     2 |    52 |     2   (0)| 00:00:01 |
|   1 |  HASH GROUP BY               |              |     2 |    52 |     2   (0)| 00:00:01 |
|*  2 |   HASH JOIN                  |              |    10 |   260 |     1   (0)| 00:00:01 |
|   3 |    TABLE ACCESS FULL         | DEPARTMENTS  |     2 |    26 |     1   (0)| 00:00:01 |
|*  4 |    HASH JOIN                 |              |    10 |   208 |     1   (0)| 00:00:01 |
|   5 |     TABLE ACCESS FULL        | EMPLOYEES    |   107 |  3210 |     1   (0)| 00:00:01 |
|   6 |     TABLE ACCESS FULL        | DEPARTMENTS  |     2 |    26 |     1   (0)| 00:00:01 |
--------------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   2 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   4 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
```

统计信息：

```
Statistics
----------------------------------------------------------
          0  recursive calls
          0  db block gets
         10  consistent gets
          0  physical reads
          0  redo size
        815  bytes sent via SQL*Net to client
        608  bytes received via SQL*Net from client
          2  SQL*Net roundtrips to/from client
          0  sorts (memory)
          0  sorts (disk)
          2  rows processed
```

#### 查询2

查询两个部门('IT'和'Sales')的部门总人数和平均工资。

```sql
SELECT d.department_name, count(e.job_id) as "部门总人数", avg(e.salary) as "平均工资"
FROM hr.departments d, hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name IN ('IT', 'Sales');
```

执行结果：

```
DEPARTMENT_NAME     部门总人数       平均工资
-------------------- ---------- ----------
IT                            5       5760
Sales                        34 8955.88235
```

执行计划：

```
Plan hash value: 4104853442

--------------------------------------------------------------------------------------
| Id  | Operation                    | Name         | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT             |              |     2 |    52 |     2   (0)| 00:00:01 |
|   1 |  HASH GROUP BY               |              |     2 |    52 |     2   (0)| 00:00:01 |
|*  2 |   HASH JOIN                  |              |    10 |   260 |     1   (0)| 00:00:01 |
|   3 |    TABLE ACCESS FULL         | DEPARTMENTS  |     2 |    26 |     1   (0)| 00:00:01 |
|*  4 |    HASH JOIN                 |              |    10 |   208 |     1   (0)| 00:00:01 |
|   5 |     TABLE ACCESS FULL        | EMPLOYEES    |   107 |  3210 |     1   (0)| 00:00:01 |
|   6 |     TABLE ACCESS FULL        | DEPARTMENTS  |     2 |    26 |     1   (0)| 00:00:01 |
--------------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   2 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
   4 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

Note
-----
   - dynamic statistics used: dynamic sampling (level=2)
```

统计信息：

```
Statistics
----------------------------------------------------------
          0  recursive calls
          0  db block gets
          9  consistent gets
          0  physical reads
          0  redo size
        815  bytes sent via SQL*Net to client
        608  bytes received via SQL*Net from client
          2  SQL*Net roundtrips to/from client
          1  sorts (memory)
          0  sorts (disk)
          2  rows processed
```

### 3. 结果分析

两个查询返回的结果相同，但查询1的执行计划中包含了一个过滤器（Filter），而查询2的执行计划中没有。这意味着查询1在执行时会先过滤出部门名称为'IT'或'Sales'的记录，然后再进行聚合操作。而查询2不需要过滤器，因为HAVING子句已经过滤了结果集。

另外，查询1的统计信息中的consistent gets为10，而查询2的为9。这意味着查询1需要从数据库中读取的块数比查询2多一个。

因此，查询2是更优的查询，它的执行计划更简单，也需要读取的块数更少。

### 4. 优化指导

我们将查询2输入到sqldeveloper的优化指导工具中，发现它没有给出任何优化建议。这是因为查询2已经是最优的查询了。

## 实验总结

本次实验通过分析SQL执行计划，执行SQL语句的优化指导，理解了分析SQL语句的执行计划的重要作用。在实验中，我们设计了自己的查询语句，并执行了两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后，我们将最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，发现查询2已经是最优的查询了，没有任何优化建议。