# 实验5：掌握Oracle包，过程和函数的使用

## 实验目标

- 理解PL/SQL语言的基本结构
- 学习PL/SQL中变量和常量的定义及其应用
- 掌握如何在Oracle中创建和使用包，过程和函数

## 实验内容

1. 以hr用户身份登录Oracle数据库。
2. 创建名为MyPack的包。
3. 在MyPack包中，创建一个名为Get_SalaryAmount的函数，该函数接收部门ID作为输入参数，并通过查询employees表来计算每个部门的总薪水。
4. 在MyPack包中，创建一个名为GET_EMPLOYEES的过程，该过程接收员工ID作为输入参数，并使用游标查询employees表，递归地找出指定员工及其所有下属和下属的下属。

递归查询的SQL语句如下：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验步骤

1. 创建名为MyPack的包：

```sql
CREATE OR REPLACE PACKAGE MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
```

1. 在MyPack包中实现函数和过程：

```sql
CREATE OR REPLACE PACKAGE BODY MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER AS
    TOTAL_SALARY NUMBER(20,2);
  BEGIN
    SELECT SUM(salary) INTO TOTAL_SALARY FROM EMPLOYEES WHERE DEPARTMENT_ID = V_DEPARTMENT_ID;
    RETURN TOTAL_SALARY;
  END;

  PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER) AS
    INDENTATION VARCHAR(2000);
  BEGIN
    INDENTATION := ' ';
    FOR EMP_REC IN (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
                    START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
                    CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
      DBMS_OUTPUT.PUT_LINE(LPAD(INDENTATION,(EMP_REC.LEVEL-1)*4,' ') || EMP_REC.EMPLOYEE_ID || ' ' || EMP_REC.FIRST_NAME);
    END LOOP;
  END;
END MyPack;
/
```

1. 测试函数Get_SalaryAmount：

```sql
SELECT department_id, department_name, MyPack.Get_SalaryAmount(department_id) AS total_salary FROM departments;
```

1. 测试过程Get_Employees：

```sql
SET SERVEROUTPUT ON;
DECLARE
  EMPLOYEE_ID NUMBER;
BEGIN
  EMPLOYEE_ID := 101;
  MYPACK.Get_Employees(EMPLOYEE_ID => EMPLOYEE_ID);
END;
/
```

## 实验总结

通过本次实验，我们了解了PL/SQL语言的基本结构，学习了如何在PL/SQL中定义和使用变量和常量，以及如何创建和使用Oracle的包，过程和函数。我们还通过实际应用，深入理解了这些知识点，为以后的学习打下了一个坚实的基础。此外，实验中的实际问题也提高了我们解决实际问题的能力。例如，Get_SalaryAmount函数让我们了解了如何利用函数来封装数据库查询操作，并返回查询结果。而GET_EMPLOYEES过程则展示了如何使用过程来封装一组操作，并利用游标和递归查询实现了复杂的业务逻辑。

在今后的学习和工作中，我们可以利用这些知识，编写出更加复杂的PL/SQL程序，解决更加复杂的实际问题。同时，通过本次实验，我们也了解了如何编写和使用包，过程和函数，这为我们在实际工作中创建和使用包，过程和函数打下了坚实的基础。