

## 实验目的

通过本次实验，学生应该掌握以下内容：

1. 创建本地角色，并授予角色相关权限；
2. 创建用户，并将角色授权给用户；
3. 设置表空间限额；
4. 测试用户权限，包括创建表、插入数据、创建视图和查询表和视图的数据等；
5. 查询表空间和数据库文件的使用情况；
6. 使用概要文件限制用户登录次数。



## 实验环境

• 操作系统：Windows 11

• 数据库: Oracle 12c



## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。



## 实验步骤

**说明**：以下所有`密码`均为`123`，`错误密码`为`111`

### 创建角色和授权权限

1. 以system用户身份登录到Oracle数据库

```sql
sqlplus system/密码@pdborcl
```



2. 创建角色con_res_role并授予connect、resource和create view权限

```sql
CREATE ROLE con_res_role;
GRANT connect, resource, CREATE VIEW TO con_res_role;
```



### 创建用户

1. 创建用户sale并设置默认表空间为users，且限额为50M，授予con_res_role角色

```sql
CREATE USER sale IDENTIFIED BY 密码 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale DEFAULT TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
```



### 测试用户操作和查询

1. 测试sale用户连接数据库并创建表、插入数据、创建视图和查询表和视图的数据

```sql
-- 使用sale用户登录
sqlplus sale/密码@pdborcl

-- 创建表
CREATE TABLE customers (id NUMBER, name VARCHAR2(50)) TABLESPACE "USERS";

-- 插入数据
INSERT INTO customers (id, name) VALUES (1, 'zhang');
INSERT INTO customers (id, name) VALUES (2, 'wang');

-- 创建视图
CREATE VIEW customers_view AS SELECT name FROM customers;

-- 给hr用户共享视图customers_view
GRANT SELECT ON customers_view TO hr;

-- 查询视图的数据
SELECT * FROM customers_view;

-- 退出用户sale登录
exit;
```



2. 使用hr用户登录数据库并查询sale授权的视图

```sql
-- 使用hr用户登录
sqlplus hr/密码@pdborcl

-- 查询视图的数据
SELECT * FROM sale.customers_view;

-- 退出用户hr登录
exit;
```



### 设置概要文件、锁定和删除

1. 设置概要文件限制sale用户最多登录3次错误密码后被锁定

```sql
-- 使用system用户登录
sqlplus system/密码@pdborcl

-- 设置概要文件
ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```



2. 测试用户锁定和解锁

```sql
-- 使用sale用户登录并输入错误密码3次，用户被锁定
sqlplus sale/错误密码@pdborcl

-- 使用system用户登录解锁sale用户
sqlplus system/密码@pdborcl
ALTER USER sale ACCOUNT UNLOCK;
```



3. 查询表空间和数据库使用情况

```sql
-- 查询表空间的数据文件及其磁盘使用情况
SELECT tablespace_name, file_name, bytes/1024/1024 MB, maxbytes/1024/1024 MAX_MB, autoextensible
FROM dba_data_files
WHERE tablespace_name = 'USERS';

-- 查询表空间的使用情况
SELECT a.tablespace_name "表空间名", Total/1024/1024 "大小MB", free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB", Round(( total - free )/ total,4)* 100 "使用率%"
FROM (SELECT tablespace_name, SUM(bytes)free FROM dba_free_space GROUP BY tablespace_name)a,
(SELECT tablespace_name, SUM(bytes)total FROM dba_data_files GROUP BY tablespace_name)b
WHERE a.tablespace_name = b.tablespace_name;

-- 查询数据库文件的使用情况
SELECT tablespace_name, SUM(bytes)/1024/1024 "已使用空间（MB）", SUM(maxbytes)/1024/1024 "最大空间（MB）", SUM(bytes)/SUM(maxbytes) "使用率"
FROM dba_data_files
GROUP BY tablespace_name;
```



4. 删除用户和角色

```sql
-- 使用system用户登录删除用户和角色
sqlplus system/密码@pdborcl
DROP USER sale CASCADE;
DROP ROLE con_res_role;
```



### 优化指导

1. 定期对表空间进行清理和整理，删除不必要的数据文件和表空间，以减少数据库的资源占用；
2. 设置数据库的容错机制，如备份和恢复，以确保数据库的安全性和可用性；
3. 对用户的密码进行加密处理，并要求用户定期更换密码，以保障数据库的安全性；
4. 对用户进行细致的权限管理，避免敏感数据被恶意访问和篡改；
5. 优化数据库的查询语句和索引等，以提高数据库的性能和响应速度。



## **实验总结**

通过本次实验，我们深入学习了Oracle数据库的用户和权限管理方面的知识，并实践了实现创建本地角色、创建用户、授权角色、设置表空间限额、测试用户权限、查询表空间和数据库使用情况等操作。

总的来说，这个实验让我们理解了Oracle数据库在用户和权限管理方面的重要性，也让我们更加清晰地掌握了数据库管理和操作方面的知识和技能。在今后的实际工作中，我们将能够更加熟练地使用这些知识和技能，提高自己的数据库管理和操作能力。