-- 创建一个程序包
CREATE OR REPLACE PACKAGE pkg_sales AS
    -- 存储过程：创建订单
    PROCEDURE create_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER);
    
    -- 函数：计算订单总价
    FUNCTION calculate_total(p_order_id IN NUMBER) RETURN NUMBER;
END pkg_sales;

-- 程序包实现
CREATE OR REPLACE PACKAGE BODY pkg_sales AS
    PROCEDURE create_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER) IS
    BEGIN
        -- 实现创建订单的逻辑
    END create_order;

    FUNCTION calculate_total(p_order_id IN NUMBER) RETURN NUMBER IS
        v_total NUMBER(10,2);
    BEGIN
        -- 实现计算订单总价的逻辑
        RETURN v_total;
    END calculate_total;
END pkg_sales;
