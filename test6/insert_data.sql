-- 插入用户数据
DECLARE
    v_id NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        v_id := i;
        INSERT INTO Users(ID, Username, Password, Email)
        VALUES (v_id, 'User'||v_id, 'Password'||v_id, 'User'||v_id||'@email.com');
    END LOOP;
    COMMIT;
END;

-- 插入商品数据
DECLARE
    v_id NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        v_id := i;
        INSERT INTO Products(ID, Name, Price, Quantity)
        VALUES (v_id, 'Product'||v_id, DBMS_RANDOM.VALUE(10, 100), DBMS_RANDOM.VALUE(1, 100));
    END LOOP;
    COMMIT;
END;
