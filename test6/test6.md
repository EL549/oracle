# 基于Oracle数据库的商品销售系统设计

## 数据库设计

### 表空间

在Oracle数据库中，创建了两个表空间：TBS1和TBS2，以支持分离和优化存储。相关的SQL脚本在文件 "create_tablespace.sql" 中。

- TBS1: 存储Users和Products表。
- TBS2: 存储Orders和OrderDetails表。

### 数据表

设计并创建了四个表，相关的SQL脚本在文件 "create_table.sql" 中：

1. Users表：存储用户信息，包括用户ID，用户名，密码和电子邮件等。用户ID设为主键，用户名设为唯一键，避免重复。

2. Products表：存储商品信息，包括商品ID，商品名称，价格和库存数量等。商品ID设为主键。

3. Orders表：存储订单信息，包括订单ID，用户ID，下单时间等。订单ID设为主键，用户ID与Users表的用户ID相关联。

4. OrderDetails表：存储订单详细信息，包括订单详细ID，订单ID，商品ID，购买数量等。订单详细ID设为主键，订单ID与Orders表的订单ID相关联，商品ID与Products表的商品ID相关联。

### 模拟数据

利用PL/SQL脚本生成了10万条模拟数据，并通过批量插入的方式将这些数据插入到四个表中。相关的SQL脚本在文件 "insert_data.sql" 中。

在Oracle中生成随机数据可以使用DBMS_RANDOM函数。以下插入了10万条用户数据和商品数据。

## 权限及用户分配

### 用户创建

在Oracle数据库中，创建了两个用户，相关的SQL脚本在文件 "user_and_privilege.sql" 中：

- user1：拥有TBS1表空间的使用权限，可以操作Users和Products表。
- user2：拥有TBS2表空间的使用权限，可以操作Orders和OrderDetails表。

### 权限分配

对用户的权限进行了精细划分：

- user1：赋予了对Users和Products表的增删改查权限。
- user2：赋予了对Orders和OrderDetails表的增删改查权限。

## 存储过程和函数

创建了名为pkg_sales的程序包，其中包含了两个处理业务逻辑的存储过程和函数，相关的SQL脚本在文件 "package.sql" 中：

- create_order：根据用户ID和商品ID创建订单，并将新创建的订单ID返回。
- calculate_total：根据订单ID计算订单的总价，并返回订单总价。

## 数据库备份方案

设计了一个基于RMAN的数据库备份策略。创建了备份脚本，可以按需调整备份的频率和目标路径，以确保数据库的安全性和数据的完整性。相关的SQL脚本在文件 "backup.sql" 中。